package com.example.demo;


import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
class ServiceTestJUnit {

    @Autowired
    private ServiceTest serviceTest;

    @Test
    void testSum() {
        int a = 3;
        int b = 4;
        assertEquals(7, serviceTest.sum(a, b));
    }
}
